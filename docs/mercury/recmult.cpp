#include "recmult.h"
#include "lrmodel.h"
#include "TROOT.h"
#include <iostream>
#include <fstream>
// #include "eiquadprog.hpp"
// #include "nnls.h"
#include <cmath>
#include <Eigen/Dense>

RecMult::RecMult(LRModel *lrm, int nvert)
{
    this->lrm = lrm;
    nsensors = lrm->GetSensorCount();
    sensor.resize(nsensors);
    Active.reserve(nsensors);
    for (int i=0; i<nsensors; i++) {
        sensor[i].x = lrm->GetX(i);
        sensor[i].y = lrm->GetY(i);
        sensor[i].gain = 1.;
        sensor[i].on = true;
    }
    A.resize(nsensors);
    sat.resize(nsensors);

    this->nvert = nvert;
    guess_x.resize(nvert);
    guess_y.resize(nvert);
    guess_e.resize(nvert);
    rec_x.resize(nvert);
    rec_y.resize(nvert);
    rec_e.resize(nvert); 
}

RecMult::RecMult(std::string json_str, int nvert) : RecMult(new LRModel(json_str), nvert)
{
    external_lrm = false;    
}

RecMult::~RecMult()
{
    if (!external_lrm) delete lrm;
}

// Check for static and dynamic passives + saturation
void RecMult::checkActive()
{
    bool act;
    Active.clear();
    int maxid = getMaxSignalID();
    double cutoff = std::max(rec_abs_cutoff, A[maxid]*rec_rel_cutoff);
    for (int i=0; i<nsensors; i++) {
// sensor is active if it's (enabled) AND (NOT saturated) AND (not below the cutoff)
// AND (within cutoff radius)
        if (sensor[i].on && !sat[i] && A[i] >= cutoff)
            Active.push_back(i);
    }
}

int RecMult::getMaxSignalID()
{
    auto strongest = std::max_element(std::begin(A), std::end(A));
    return std::distance(std::begin(A), strongest);
}

double RecMult::getDistFromSensor(int id, double x, double y)
{
  return hypot(x-sensor[id].x, y-sensor[id].y);
}

// ============= Cost functions =================

double RecMult::getChi2(std::vector <double> x, std::vector <double> y, std::vector <double> energy, bool weighted)
{
    double sum = 0;

    for (int i : Active) {
        double sumLRF = 0;
        for (int j=0; j<nvert; j++) {
            double LRFhere = lrm->Eval(i, x[j], y[j], 0.) * energy[j]; // LRF(X, Y, Z) * energy;
    //        std::cout << i << " " << LRFhere << std::endl;
            if (LRFhere <= 0.)
                return LastMiniValue *= 1.25; //if LRFs are not defined for this coordinates
            sumLRF += LRFhere;    
        }
        double delta = (sumLRF - A[i]);
        sum += weighted ? delta*delta/sumLRF : delta*delta;
    }
//    std::cout << "Sum: " << sum << std::endl;
    return LastMiniValue = sum;
}

double RecMult::getChi2autoE(std::vector <double> x, std::vector <double> y, bool weighted)
{
//    std::cout << "X:" << x << "    Y:" << y << "   E:" << energy << std::endl;    
    double sum = 0;
    size_t nact = Active.size();

    Eigen::MatrixXd a(nact, nvert); // response matrix for given positions
    Eigen::VectorXd b(nact);        // vector of active sensor outputs
    Eigen::VectorXd energy(nvert);  // vector of energies (solution)

    for (int ic=0; ic<nvert; ic++) {
        std::vector <double> lrf = lrm->EvalList(Active, x[ic], y[ic], 0.);
        for (size_t ir=0; ir<nact; ir++) {
            if (lrf[ir] <= 0)
                return LastMiniValue *= 1.25; // if any of the LRFs not defined for these coordinates
            a(ir, ic) = lrf[ir];
        }
    }
    for (size_t ir=0; ir<nact; ir++)
        b(ir) = A[Active[ir]];

    // solve the system using QR decomposition
    energy = a.colPivHouseholderQr().solve(b);
    for (int i=0; i<nvert; i++)
        rec_e[i] = energy(i);

    // return squared residual
    Eigen::VectorXd r = a*energy - b;
    return LastMiniValue = r.squaredNorm();
}

double RecMult::getLogLH(std::vector <double> x, std::vector <double> y, std::vector <double> energy)
{
    double sum = 0;

    for (int i : Active) {
        double sumLRF = 0;
        for (int j=0; j<nvert; j++) {
            double LRFhere = lrm->Eval(i, x[j], y[j], 0.) * energy[j]; // LRF(X, Y, Z) * energy;
    //        std::cout << i << " " << LRFhere << std::endl;
            if (LRFhere <= 0.)
                return LastMiniValue *= 1.25; //if LRFs are not defined for this coordinates
            sumLRF += LRFhere;    
        }

        sum -= A[i]*log(sumLRF) - sumLRF; // measures probability
    }

    return LastMiniValue = sum;
}
/*
double RecMult::getLogLHautoE(std::vector <double> x, std::vector <double> y)
{
//    std::cout << "X:" << x << "    Y:" << y << "   E:" << energy << std::endl;    
    double sum = 0;
    double sumsig = 0;
    double sumlrf = 0;
    size_t len = Active.size();

    std::vector <double> lr = lrm->EvalList(Active, x, y, z);
    for (size_t i=0; i<len; i++) {
        sumsig += A[Active[i]];
        sumlrf += lr[i];
    }
    double energy = sumsig/sumlrf;
    
    for (size_t j=0; j<len; j++) {
        double LRFhere = lr[j]*energy; // LRF(X, Y, Z) * energy;
        if (LRFhere <= 0.)
            return LastMiniValue *= 1.25; //if LRFs are not defined for this coordinates

        sum -= A[Active[j]]*log(LRFhere) - LRFhere; // measures probability
    }

    return LastMiniValue = sum;
}
*/
// =============== Minuit 2 ==================
RecMultMinuit::RecMultMinuit(LRModel *lrm, int nvert) : RecMult(lrm, nvert)
{
    RootMinimizer = new ROOT::Minuit2::Minuit2Minimizer(ROOT::Minuit2::kMigrad);
    //RootMinimizer = new ROOT::Minuit2::Minuit2Minimizer(ROOT::Minuit2::kSimplex);
    RootMinimizer->SetMaxFunctionCalls(RMmaxFuncCalls);
    RootMinimizer->SetMaxIterations(RMmaxIterations);
    RootMinimizer->SetTolerance(RMtolerance);

// Set Minuit2 and ROOT verbosity level
    RootMinimizer->SetPrintLevel(MinuitPrintLevel);
    gErrorIgnoreLevel = RootPrintLevel;

//    std::cout << "w:" << fWeighted << ", e: " << fAutoE << std::endl;
}

RecMultMinuit::RecMultMinuit(std::string json_str, int nvert) : RecMultMinuit(new LRModel(json_str), nvert)
{
    external_lrm = false;   
}

RecMultMinuit::~RecMultMinuit()
{
    delete RootMinimizer;
    if (init_done)
        delete FunctorLSML;
}

void RecMultMinuit::setMinuitVerbosity(int val) 
{
    MinuitPrintLevel = val; 
    if (RootMinimizer)
        RootMinimizer->SetPrintLevel(val);
}

bool RecMultMinuit::ProcessEvent(std::vector <double> &a, std::vector <bool> &sat)
{
    if (!init_done) {
        InitCostFunction();
        RootMinimizer->SetFunction(*FunctorLSML);
        std::cout << "Ndim: " << RootMinimizer->NDim() << std::endl;
        init_done = true;
    }

    if (a.size() < nsensors || sat.size() < nsensors)
        return false;
    for (int i=0; i<nsensors; i++) {
        A[i] = a[i]/sensor[i].gain;
        this->sat[i] = sat[i];
    }

// determine active sensors and see if there are enough for reconstruction
    checkActive();
    rec_dof = Active.size() - nvert*3;
    if (rec_dof < 1) {
        rec_status = 6;
        return false;
    }

// set initial variables to minimize
// NB: the variables must be initialized in same order as they passed to the cost funtion !!! 
    for (int i=0; i<nvert; i++) {
//        std::cout << "==== Guess "<< i+1 << ": " << guess_x[i] << " " << guess_y[i] << " " << guess_e[i] << std::endl;

        RootMinimizer->SetVariable(i*2, std::string("x")+std::to_string(i), guess_x[i], RMstepX);
        RootMinimizer->SetVariable(i*2+1, std::string("y")+std::to_string(i), guess_y[i], RMstepY); 
    }
    if (!fAutoE)
        for (int i=0; i<nvert; i++) {
            //    double step_e = RMstepEnergy > 0 ? RMstepEnergy : guess_e[i]*0.2;
            RootMinimizer->SetLowerLimitedVariable(nvert*2+i, std::string("e")+std::to_string(i), guess_e[i], guess_e[i]*0.1, 1.0e-6);
    }


    // do the minimization
    bool fOK = false;
    fOK = RootMinimizer->Minimize();

    if (fOK) {
        rec_status = 0 ;		// Reconstruction successfull

        const double *xs = RootMinimizer->X();

        for (int i; i<nvert; i++) {
            rec_x[i] = xs[i*2];
            rec_y[i] = xs[i*2+1];
            if (!fAutoE)
                rec_e[i] = xs[nvert*2+i];
        }
        if (fAutoE) {
            getChi2autoE(rec_x, rec_y, true); // ToDo: treat weighted correctly
        }
//        rec_e = fAutoE ? getSumActiveSignal()/getSumActiveLRF(rec_x, rec_y, 0.) : xs[2];
        rec_min = RootMinimizer->MinValue();

    // Calc Hessian matrix and get status
        int ndim = RootMinimizer->NDim();
        double cov[ndim*ndim];
        RootMinimizer->Hesse();
        RootMinimizer->GetCovMatrix(cov);
        for (int i=0; i<nvert; i++) {
            std::vector <double> t;
            for (int j=0; j<nvert; j++)
                t.push_back(cov[i*nvert+j]);
            cov_matrix.push_back(t);
            t.clear();
        }
        return true;
    } else {
        rec_status = RootMinimizer->Status(); // reason why it has failed
        return false;
    }
}

// ============== Least Squares ===============
RecMultLS::RecMultLS(LRModel *lrm, int nvert, bool weighted) : RecMultMinuit(lrm, nvert)
{
    fWeighted = weighted;
}

RecMultLS::RecMultLS(std::string json_str, int nvert, bool weighted) : RecMultLS(new LRModel(json_str), nvert) {}

void RecMultLS::InitCostFunction()
{
    FunctorLSML = new ROOT::Math::Functor(this, &RecMultLS::Cost, fAutoE ? nvert*2 : nvert*3);
    LastMiniValue = 1.e6; //reset for the new event
}

double RecMultLS::Cost(const double *p) // 0-x, 1-y, 2-energy
{
    std::vector <double> x(nvert);
    std::vector <double> y(nvert);
    std::vector <double> e(nvert);
    for (int i; i<nvert; i++) {
        x[i] = p[i*2];
        y[i] = p[i*2+1];
        if (!fAutoE)
            e[i] = p[nvert*2+i];
    }
//    return getChi2(x, y, e, fWeighted);
    return fAutoE ? getChi2autoE(x, y, fWeighted) : getChi2(x, y, e, fWeighted);
}

// ============== Maximum Likelihood ===============
RecMultML::RecMultML(LRModel *lrm, int nvert) : RecMultMinuit(lrm, nvert) {}

RecMultML::RecMultML(std::string json_str, int nvert) : RecMultML(new LRModel(json_str), nvert) {}

void RecMultML::InitCostFunction()
{
    FunctorLSML = new ROOT::Math::Functor(this, &RecMultML::Cost, fAutoE ? nvert*2 : nvert*3);
    LastMiniValue = 1.e100; // reset for the new event
}

double RecMultML::Cost(const double *p) // 0-x, 1-y, 2-energy
{
    std::vector <double> x(nvert);
    std::vector <double> y(nvert);
    std::vector <double> e(nvert);
    for (int i; i<nvert; i++) {
        x[i] = p[i*2];
        y[i] = p[i*2+1];
        e[i] = p[nvert*2+i];
    }
    return getLogLH(x, y, e);

//    return fAutoE ? getLogLHautoE(p[0], p[1], 0.) : getLogLH(p[0], p[1], 0., p[2]);
}

#ifdef NNLS
// =================== NNLS ====================== 
RecNNLS::RecNNLS(LRModel *lrm) : RecMult(lrm) {}

RecNNLS::RecNNLS(std::string json_str) : RecNNLS(new LRModel(json_str)) {}

// initialize solution grid for a circular region 
// with the center at (x0, y0) and the radius r
int RecNNLS::InitCirc(double x0, double y0, double r, double step, int ovs)
{
    Lx.clear();
    Ly.clear();

// make grid
    int nmax = r/step;
    for (int ix = -nmax; ix <= nmax; ix++)
        for (int iy = -nmax; iy <= nmax; iy++) {
            double x = ix*step;
            double y = iy*step;
            if (sqrt(x*x+y*y) >= r)
                continue;
            Lx.push_back(x0+x);
            Ly.push_back(y0+y);
    }

// resize vectors and matrix
    lflen = Lx.size();
    x.resize(lflen);
    y.resize(nsensors);
    L.resize(nsensors, lflen);
// fill light response matrix
    for (int i=0; i<lflen; i++)
        for (int j=0; j<nsensors; j++)
            L(j,i) = lrm->Eval(j, Lx[i], Ly[i], 0);

    LL = (double**)malloc(lflen * sizeof(double*));
    for (int i=0; i<lflen; i++) {
        LL[i] = (double*)malloc(nsensors * sizeof(double));
        for (int j=0; j<nsensors; j++)
            LL[i][j] = L(j,i);
    }
   
    return lflen;    
}

bool RecNNLS::ProcessEvent(std::vector <double> &a, std::vector <bool> &sat)
{
    if (a.size() < nsensors || sat.size() < nsensors)
        return false;
    for (int i=0; i<nsensors; i++) {
        A[i] = a[i]/sensor[i].gain;
        this->sat[i] = sat[i];
    }

//    double xx[lflen];
    std::vector <double> xxx(lflen);
    double *xx = &xxx[0];

    for (int i=0; i<lflen; i++)
        xx[i] = 0;

// ToDo: remove saturated!   

// NNLS fit

    std::cout << "in\n";
    int status = nnls(LL, nsensors, lflen, &A[0], xx, NULL, NULL, NULL, NULL);
    std::cout << "out\n";
    
    if (status) {
        rec_status = -5;
        return false;
    }

// argmax
    std::vector<double>::iterator maxpos = max_element(xxx.begin(), xxx.end());
    int imaxlf = std::distance(xxx.begin(), maxpos);
    double maxlf = xxx[imaxlf];
/*    
// second largest
    xxx[imaxlf] = 0.;
    std::vector<double>::iterator maxpos2 = max_element(xxx.begin(), xxx.end());
    int imaxlf2 = std::distance(xxx.begin(), maxpos2);
    double maxlf2 = xxx[imaxlf2];

    rec_x = (maxlf*Lx[imaxlf] + maxlf2*Lx[imaxlf2])/(maxlf+maxlf2);
    rec_y = (maxlf*Ly[imaxlf] + maxlf2*Ly[imaxlf2])/(maxlf+maxlf2);;
*/

// return the coordinates of the grid node with the largest value
    rec_x = Lx[imaxlf];
    rec_y = Ly[imaxlf];
    rec_status = 0;
    return true;
}
/*
bool RecNNLS::ProcessEvent(std::vector <double> &a, std::vector <bool> &sat)
{
    if (a.size() < nsensors || sat.size() < nsensors)
        return false;
    for (int i=0; i<nsensors; i++) {
        A[i] = a[i]/sensor[i].gain;
        this->sat[i] = sat[i];
    }

    for (int i=0; i<nsensors; i++)
        y(i) = A[i];

// ToDo: remove saturated!   

// NNLS fit

    std::cout << "in\n";
    bool status = Eigen::NNLS<Eigen::MatrixXd>::solve(L, y, x);
    std::cout << "out\n";

    if (!status) {
        rec_status = -5;
        return false;
    }

    int imaxlf;
    x.maxCoeff(&imaxlf); 

    rec_x = Lx[imaxlf];
    rec_y = Ly[imaxlf];
    rec_status = 0;
    return true;
}
*/
#endif