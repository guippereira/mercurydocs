#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <array>
#include "lrmodel.h"
#include "lrfaxial.h"
#include "reconstructor.h"
#include "reconstructor_mp.h"
#include <cmath>

#define USE_OMP

// The light response model of the detector (LRM) is stored in a dedicated class (LRModel)
// LRM = PMT positions + coordinate transforms + LRFs
// The LRM cam be loaded from and saved to JSON format

int main()
{
// 1. Load LRM for LZ top array from a file
// The file contains the LRFs used in MDC3 analysis
    std::ifstream t("data/LRM_LZtop.json");
        if (!t.good()) {
        std::cout << "Couldn't find model file (data/LRM_LZtop.json)" << std::endl;
        return 1;
    }
    std::stringstream buffer;
    buffer << t.rdbuf();
    
    LRModel lrm(buffer.str());
    
//    LRFaxial *qqq = dynamic_cast<LRFaxial*>(lrm.GetLRF(0));
//    qqq->setNonNegative(true);
//    qqq->SetFlatTop(true);

// 2. Load flood
    // format: a0, ... a253, x, y, z, bsum
    // channel saturation is indicated by flipping the sign of the area
    std::vector <std::vector <double> > Data;
    std::vector <std::vector <bool> > Sat;

    std::ifstream f("data/Kr83m-1kevt.txt");
    if (!f.good()) {
        std::cout << "Couldn't find data file (data/Kr83m-1kevt.txt)" << std::endl;
        return 1;
    }
    std::string line;
    int cnt = 0;
    while (std::getline(f, line)) {
        std::vector <double> evt;
        std::vector <bool> sat(253);

        std::istringstream iss(line); //put line into stringstream
        double val;
        while(iss >> val) //read word by word
            evt.push_back(val);

        for (int j=0; j<253; j++) // take care of the saturated channels
            if (sat[j] = (evt[j] < 0.))
                evt[j] = -evt[j];

        Data.push_back(evt);
        Sat.push_back(sat);
        evt.clear();
        if (++cnt > 10000) break;
    }
/*
// 2a.  Load more sophisticated flood
    // format: a0, ... a253, x, y, z, bsum
    // channel saturation is indicated by flipping the sign of the area
    std::vector <std::vector <double> > Data;
    std::vector <std::vector <bool> > Sat;

    std::ifstream f("/media/vova/big_data/Current-Backup/Work/LZ/Kr_lzap483-2.txt");

    if (!f.good()) {
        std::cout << "Couldn't find data file" << std::endl;
        return 1;
    }
    std::string line;
    int cnt = 0;
    while (std::getline(f, line)) {
        std::vector <double> row;
        std::vector <double> evt(255);
        std::vector <bool> sat(253);

        std::istringstream iss(line); //put line into stringstream
        double val;
        while(iss >> val) //read word by word
            row.push_back(val);

        for (int j=0; j<253; j++) { // take care of the saturated channels
            evt[j] = row[j+494];
            if (sat[j] = (evt[j] < 0.))
                evt[j] = -evt[j];
        }
        evt[253] = row[988];
        evt[254] = row[989];

        Data.push_back(evt);
        Sat.push_back(sat);
        evt.clear();
        if (++cnt > 5000) break;
    }
*/
// 3. Reconstruct the flood events

    RecLS reco(&lrm);
    reco.setCogRelCutoff(0.1);
    reco.setRecCutoffRadius(400.);

// begin by reconstructing and printing results for the first 10 events
    std::cout << "Here are the first 10 recnstructed events:" << std::endl;
    for (int i=0; i<10; i++) {
        reco.ProcessEvent(Data[i], Sat[i]);
        std::cout << "Event " << i << " status=" << reco.getRecStatus() << std::endl;
        std::cout << "X " << Data[i][253]*10. << " vs " << reco.getRecX() << std::endl;
        std::cout << "Y " << Data[i][254]*10. << " vs " << reco.getRecY() << std::endl;
        std::cout << "E " << reco.getRecE() << std::endl;
        std::cout << "Eguess " << reco.getGuessE() << std::endl << std::endl;
    }

// and then reconstruct the whole flood and dump the result into a file
    std::ofstream recfile;

#ifdef USE_OMP
    recfile.open ("rec_out_mp.txt", std::ios_base::app);
    RecLS_MP reco_mp(&lrm, 4); // use 4 threads
    reco_mp.setCogRelCutoff(0.1);
    reco_mp.setRecCutoffRadius(400.);   

    reco_mp.ProcessEvents(Data, Sat);
    std::vector <double> X = reco_mp.getRecX();
    std::vector <double> Y = reco_mp.getRecY();
    std::vector <int> Stat = reco_mp.getRecStatus(); 

    for (size_t i=0; i<Data.size(); i++) {
        if (Stat[i])
            continue;
        recfile << Data[i][253]*10 << " " << X[i] << " " << Data[i][254]*10 << " " << Y[i] << std::endl;
    } 
#else
    recfile.open ("rec_out.txt", std::ios_base::app);
    for (size_t i=0; i<Data.size(); i++) {
        reco.ProcessEvent(Data[i], Sat[i]);
        if (reco.getRecStatus())
            continue;
        recfile << Data[i][253]*10 << " " << reco.getRecX() << " " << Data[i][254]*10 << " " << reco.getRecY() << std::endl;
    }
#endif    
    recfile.close();

    return 0;
}
