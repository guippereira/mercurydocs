#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <array>
#include "lrmodel.h"
#include "lrfaxial.h"
#include "reconstructor.h"
#include "reconstructor_mp.h"
#include "recmult.h"
#include <cmath>

// The light response model of the detector (LRM) is stored in a dedicated class (LRModel)
// LRM = PMT positions + coordinate transforms + LRFs
// The LRM cam be loaded from and saved to JSON format

int main()
{
// 1. Load LRM for LZ top array from a file
// The file contains the LRFs used in MDC3 analysis
    std::ifstream t("data/LRM_LZtop.json");
        if (!t.good()) {
        std::cout << "Couldn't find model file (data/LRM_LZtop.json)" << std::endl;
        return 1;
    }
    std::stringstream buffer;
    buffer << t.rdbuf();
    
    LRModel lrm(buffer.str());
    
// 2a.  Load flood
    // format: s1top[253], s1bot[241], s2top[253], s2bot[241] x, y, z, s2xyarea, dtime
    // channel saturation is indicated by flipping the sign of the area
    std::vector <std::vector <double> > Data;
    std::vector <std::vector <bool> > Sat;

    std::ifstream f("/media/vova/big_data/Current-Backup/Work/LZ/Kr_lzap483-2.txt");

    if (!f.good()) {
        std::cout << "Couldn't find data file" << std::endl;
        return 1;
    }
    std::string line;
    int cnt = 0;
    while (std::getline(f, line)) {
        std::vector <double> row;
        std::vector <double> evt(255);
        std::vector <bool> sat(253);

        std::istringstream iss(line); //put line into stringstream
        double val;
        while(iss >> val) //read word by word
            row.push_back(val);

        for (int j=0; j<253; j++) { // take care of the saturated channels
            evt[j] = row[j+494];
            if (sat[j] = (evt[j] < 0.))
                evt[j] = -evt[j];
        }
        evt[253] = row[988];
        evt[254] = row[989];

        Data.push_back(evt);
        Sat.push_back(sat);
        evt.clear();
        if (++cnt > 1000) break;
    }

// 3. Reconstruct artificially created double events

    RecLS reco(&lrm);
//    RecNNLS reco(&lrm);
    reco.setCogRelCutoff(0.1);
    reco.setRecCutoffRadius(400.);

    RecMultLS reco2(&lrm, 2);
    reco2.setMinuitVerbosity(0);
    reco2.setRMtolerance(0.1);
    reco2.setRMmaxFuncCalls(2000);
    reco2.setRMmaxIterations(2000);
    reco2.setRecRelCutoff(0.05);
    reco2.setAutoE(false);
    std::cout << "Here are the first 20 doubles:" << std::endl;
    std::vector <bool> all_good(253, false);
    std::vector <double> Krx2(253);
    std::vector <bool> Satx2(253);
    for (int i=0; i<20; i++) {
        for (int j=0; j<253; j++) {
            Krx2[j] = Data[i*2][j] + Data[i*2+1][j];
            Satx2[j] = Sat[i*2][j] || Sat[i*2+1][j];
        }
        std::vector <double> xg(2), yg(2), eg(2);
        xg[0] = Data[i*2][253]*10.+20.;
        yg[0] = Data[i*2][254]*10.+20.;
        reco.ProcessEvent(Data[i*2], Sat[i*2]);
        eg[0] = reco.getRecE();

        xg[1] = Data[i*2+1][253]*10.+20;
        yg[1] = Data[i*2+1][254]*10.+20.;
        reco.ProcessEvent(Data[i*2+1], Sat[i*2+1]);
        eg[1] = reco.getRecE();

        reco2.setGuessPosition(xg, yg);
        reco2.setGuessEnergy(eg);

        reco2.ProcessEvent(Krx2, Satx2);
        std::cout << "Event " << i << " status=" << reco2.getRecStatus() << std::endl;
        std::cout << "dof " << reco2.getDof() << std::endl;
        std::cout << "X1 " << Data[i*2][253]*10. << " vs " << reco2.getRecX()[0] << std::endl;
        std::cout << "Y1 " << Data[i*2][254]*10. << " vs " << reco2.getRecY()[0] << std::endl;
        std::cout << "E1 " << reco2.getRecE()[0] << std::endl;
        std::cout << "X2 " << Data[i*2+1][253]*10. << " vs " << reco2.getRecX()[1] << std::endl;
        std::cout << "Y2 " << Data[i*2+1][254]*10. << " vs " << reco2.getRecY()[1] << std::endl;
        std::cout << "E2 " << reco2.getRecE()[1] << std::endl;
//        std::cout << "Eguess " << reco.getGuessE() << std::endl << std::endl;
    } 

    return 0;   

// and then reconstruct the whole flood and dump the result into a file
    std::ofstream recfile;
    recfile.open ("rec_out.txt", std::ios_base::app);

    RecLS_MP reco_mp(&lrm, 4);
    reco_mp.setCogRelCutoff(0.1);
    reco_mp.setRecCutoffRadius(400.);   

    reco_mp.ProcessEvents(Data, Sat);
    std::vector <double> X = reco_mp.getRecX();
    std::vector <double> Y = reco_mp.getRecY();
    std::vector <int> Stat = reco_mp.getRecStatus(); 

    for (size_t i=0; i<Data.size(); i++) {
        if (Stat[i])
            continue;
        recfile << Data[i][253]*10 << " " << X[i] << " " << Data[i][254]*10 << " " << Y[i] << std::endl;
    } 
/*
    for (size_t i=0; i<Data.size(); i++) {
        reco.ProcessEvent(Data[i], Sat[i]);
        if (reco.getRecStatus())
            continue;
        recfile << Data[i][253]*10 << " " << reco.getRecX() << " " << Data[i][254]*10 << " " << reco.getRecY() << std::endl;
    }
*/    
    recfile.close();

    return 0;
}
