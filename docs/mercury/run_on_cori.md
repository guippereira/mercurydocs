## Use Mercury Standalone on Cori

Start shifter

`shifter --module=cvmfs --image=luxzeplin/offline_hosted:centos7 bash --rcfile ~/.bashrc.ext`

The rest is done at shifter prompt
```
source /cvmfs/lz.opensciencegrid.org/LZap/release-4.8.3/x86_64-centos7-gcc7-opt/setup.sh
git clone https://gitlab.com/vovalz/mercury.git
cd mercury
```

Substitute Makefile for one with the correct settings for Cori
```
mv Makefile Makefile.bak
mv Makefile.cori Makefile
```

Compile and run the example
```
make example
./example
```

example.cpp contains the code for tests in single and multiprocessor modes. Uncomment the line 

`#define USE_OMP`

to use multiple threads (4 by default). 

