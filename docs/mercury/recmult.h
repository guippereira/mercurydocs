#ifndef RECMULT_H
#define RECMULT_H

#include <vector>
#include <string>
#include <Eigen/Dense>
#include "TMath.h"
#include "Math/Functor.h"
#include "Minuit2/Minuit2Minimizer.h"
#include "lrmodel.h"

class LRModel;

class RecMult
{
    struct RecSensor
    {
        double x;
        double y;
        double gain;
        bool on;
    };
public:
    RecMult(LRModel *lrm, int nvert);
    RecMult(std::string json_str, int nvert);
    virtual ~RecMult();

    virtual bool ProcessEvent(std::vector <double> &a, std::vector <bool> &sat) = 0;

// cost functions
    double getChi2(std::vector <double> x, std::vector <double> y, std::vector <double> energy, bool weighted = true);
    double getChi2autoE(std::vector <double> x, std::vector <double> y, bool weighted);
    double getLogLH(std::vector <double> x, std::vector <double> y, std::vector <double> energy);
//    double getLogLHautoE(std::vector <double> x, std::vector <double> y);

// tracking of minimized value
    double LastMiniValue;

// public interface
public:
    int getRecStatus() {return rec_status;}
    int getDof() {return rec_dof;}
    std::vector <double> getRecX() {return rec_x;}
    std::vector <double> getRecY() {return rec_y;}
    std::vector <double> getRecE() {return rec_e;}
    double getRecMin() {return rec_min;}
    double getChi2Min() {return getChi2(rec_x, rec_y, rec_e);}
    std::vector <std::vector <double> > getCovMatrix() {return cov_matrix;}

    void setRecAbsCutoff(double val) {rec_abs_cutoff = val;}
    void setRecRelCutoff(double val) {rec_rel_cutoff = val;}
    void setRecCutoffRadius(double val) {rec_cutoff_radius = val;}
    void setEnergyCalibration(double val) {ecal = val;}
//    void setGain(int id, double val) {sensor.at(id).gain = val;}
    void setGuessPosition(std::vector <double> x, std::vector <double> y) 
            {guess_x = x; guess_y = y;}
    
    void setGuessEnergy(std::vector <double> e) {guess_e = e; guess_e_auto = false;}
//    void setGuessEnergyAuto() {guess_e_auto = true;}

    LRModel *getLRModel() {return lrm;}
    std::string getLRModelJson() {return lrm->GetJsonString();}

protected:    
    void checkActive();
    int getMaxSignalID();
    double getDistFromSensor(int id, double x, double y);

protected:
    LRModel *lrm;
    bool external_lrm = true;
    int nsensors = 0;
    int nvert = 0;
// cached sensor parameters
    std::vector <RecSensor> sensor;
    std::vector <int> Active;
// cached input parameters
    std::vector <double> A;
    std::vector <bool> sat;

// initial guess
    bool guess_e_auto = true;
    std::vector <double> guess_x;
    std::vector <double> guess_y;
    std::vector <double> guess_e;
    double ecal = 3.75e-5; // approximate scaling factor between SumSignal and energy

// dynamic passives
    double rec_cutoff_radius = 1.0e12; // all by default
    double rec_abs_cutoff = 0.;
    double rec_rel_cutoff = 0.;

// reconstruction result
    int rec_status;         // returned status of reconstruction
    std::vector <double> rec_x;			// reconstructed X position
    std::vector <double> rec_y;			// reconstructed Y position
    std::vector <double> rec_e;           // reconstructed energy
    double rec_min;         // reduced best chi-squared from reconstruction
    int rec_dof;			// degrees of freedom

    std::vector <std::vector <double> > cov_matrix;
};

class RecMultMinuit : public RecMult
{
public:
    RecMultMinuit(LRModel *lrm, int nvert);
    RecMultMinuit(std::string json_str, int nvert);
    virtual ~RecMultMinuit();
    virtual void InitCostFunction() = 0;
    virtual bool ProcessEvent(std::vector <double> &a, std::vector <bool> &sat);

    void setRMstepX(double val) {RMstepX = val;}
    void setRMstepY(double val) {RMstepY = val;}
    void setRMstepEnergy(double val) {RMstepEnergy = val;}
    void setRMmaxFuncCalls(int val) {RootMinimizer->SetMaxFunctionCalls(RMmaxFuncCalls = val);}
    void setRMmaxIterations(int val) {RootMinimizer->SetMaxIterations(RMmaxIterations = val);}
    void setRMtolerance(double val) {RootMinimizer->SetTolerance(RMtolerance = val);}
    void setMinuitVerbosity(int val);
    void setAutoE(bool val) {fAutoE = val;}

protected:
// ROOT/Minuit stuff
    ROOT::Math::Functor *FunctorLSML = nullptr;
    ROOT::Minuit2::Minuit2Minimizer *RootMinimizer = nullptr;
// initial steps
    double RMstepX = 1.;
    double RMstepY = 1.;
    double RMstepEnergy = 0.;
// control over MINUIT2 stopping conditions
    int RMmaxFuncCalls = 500;       // Max function calls
    int RMmaxIterations = 1000; 	// Max iterations
    double RMtolerance = 0.001;		// Iteration stops when the function is within <tolerance> from the (estimated) min/max
// control over ROOT/MINUIT2 output
    int MinuitPrintLevel = 0;       // MINUIT2 messages
    int RootPrintLevel = 1001;      // ROOT messsages   

public:
    bool init_done = false;
    bool fAutoE = false;
};

class RecMultLS : public RecMultMinuit
{
public:
    RecMultLS(LRModel *lrm, int nvert, bool weighted = true);
    RecMultLS(std::string json_str, int nvert, bool weighted = true);
    virtual void InitCostFunction();
    double Cost(const double *p);
    bool fWeighted = true;
};

class RecMultML : public RecMultMinuit
{
public:
    RecMultML(LRModel *lrm, int nvert);
    RecMultML(std::string json_str, int nvert);
    virtual void InitCostFunction();
    double Cost(const double *p);
};

#endif // RECMULT_H
