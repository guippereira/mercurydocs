Splines
=====

Mercury includes a sub-project *spline123* for fitting data with smooth cubic functions in 1, 2 and 3 dimensions. It was developed to provide the basic building blocks of LRFs, but at the same time proved to be a convenient tool for implementing various spatial- and time-dependent corrections in a simple way. A spline is characterized by the minimum and the maximum (the limits) and the number of intervals along each axis, while its shape is determined by a set of spline coefficients. A complete spline can be exported to or imported from a JSON string. For example, the alpha-derived corrections to S1 and S2 are supplied as a set of files, each containing a JSON string that describes the respective spline. 

It is also possible to fit the available data in 1, 2 or 3 dimensions with a spline using helper classes :code:`BSFitXD` and :code:`ConstrainedFitXD`, where X stands for 1, 2 or 3. These classes are first initialized with the limits and the number of intervals along each axis. The number of intervals controls the balance between the ability of the spline to faithfully reproduce the data and its smoothness and should be carefully chosen to avoid both under- and over-fitting. A bit of trial and error can be required to achieve a good fit.

The constrained fit classes provide an additional control over the result of the fit. In most cases, the constraints are used to ensure that the resulting spline is non-negative and, possibly, monotonic (non-increasing or non-decreasing). It is also possible to set minimum and maximum limits (in fact, non-negative is a special case of a minimum limit) or fix the value of a spline or its derivative at a certain point. A typical use case for a fixed value is when you are fitting a time-dependent correction and want to make sure it is equal to unity at the start.

The spline package offers three main options for fitting the data: simple, weighted and binned. In a simple fit, each provided data point is used in the fit with equal weight. In a weighted fit, a weight is specified for each data point, which can be useful if one wants to take uncertainties into account. For large datasets, the recommended fit is binned, where the data points are grouped in the bins along the coordinate axis (similarly to a profile histogram in ROOT) and the fit is done over the mean values of the data points in each bin. The weight of a bin is equal to the number of the data points in it. The fitting algorithm tries to find a solution that minimizes the second derivative for the bins with no data points, thus suppressing unwanted oscillations of the resulting spline. This also allows fitting of a round TPC cross section with a rectangular 2D spline. In fact, it looks like the quality of such a fit benefits from making the spline limits wide enough to cover the TPC cross section plus some margin (approximately equal to one spline interval) on each side.

A typical spline fitting workflow is as follows:

#. create a fit object (BSFitXD or ConstrainedFitXD):

    .. code-block:: c++

        ConstrainedFit2D cfit = new ConstrainedFit2D(Xmin, Xmax, BinsX, Ymin, Ymax, YinsX);
    
#. in the case of ConstrainedFitXD, define the constraints:

    .. code-block:: c++

        cfit.ForceNonNegative(); //force the results of the fit to be composed of only positive values.
        cfit.ForceFlatTopX(); // force a null first derivative at the function maximum


#. use :code:`AddData()` function to fill the object with the data to fit (can be called several times):

    .. code-block:: c++
        cfit.AddData(X, Y, V) // add a data point at (x, y) with value of V

#. in the case of a binned fit perform the fit with the :code:`BinnedFit()` function
  
#. alternatively, in the case of a not binned fit, perform the fit with the :code:`Fit()` or the :code:`WeightedFit()` function.

#. The fit status is returned by :code:`BinnedFit()`, :code:`Fit()` or :code:`WeightedFit()`, as a boolean flag with :code:`false`` indicating a failure.

#.  if the fit was successful, the resulting spline is obtained with :code:`MakeSpline()` function. 

    .. code-block:: c++
        bool results = cfit.BinnedFit()
        if(results)
            cout << "All Good" << endl;
            Bspline1d spl = cfit.MakeSpline()
            cout << spl.Eval(100, 200) << endl; //Evaluate the fit results at (x=100, y=200)
        else
            cout << "Fit failed" << endl;


#. optionally, export JSON description from this spline to share or reuse later

    .. code-block:: c++ 
        string to_file = spl.GetJsonString()

#. if reusing the object for another binned fit, don't forget to call ClearData() before adding more data:

    .. code-block:: c++ 
        cfit.ClearData()


