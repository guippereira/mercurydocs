# Configuration file for the Sphinx documentation builder.

# -- Project information

project = 'Mercury'
copyright = '2022, LIP'
author = 'LIP'

release = '0.1'
version = '0.1.0'

# -- General configuration

extensions = [
    'sphinx_rtd_theme',
    'sphinx.ext.duration',
    'sphinx.ext.doctest',
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.intersphinx',
    'breathe',
    'sphinxcontrib.bibtex'
]
bibtex_bibfiles = ['refs.bib']
bibtex_encoding = 'latin'
bibtex_default_style = 'unsrt'


breathe_default_project = "mercury"

intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
    'sphinx': ('https://www.sphinx-doc.org/en/master/', None),
}
intersphinx_disabled_domains = ['std']

templates_path = ['_templates']

# -- Options for HTML output

html_theme = 'sphinx_rtd_theme'

# -- Options for EPUB output
epub_show_urls = 'footnote'

#breathe_projects = {
#    "mercury": "../../mercury/xml/",
#}

breathe_projects_source = {
    "mercury":  ( "../mercury/", ["reconstructor.h"] )
}

# -- breather setup


#import subprocess, os
#read_the_docs_build = os.environ.get('READTHEDOCS', None) == 'True'
#if read_the_docs_build:
#     subprocess.call('cd ../doxygen; doxygen', shell=True)
