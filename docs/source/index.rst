Welcome to Mercury's documentation!
===================================

A little history
----------
In the late 1950s, Hal Anger developed the first position sensitive scintillation detector, a scintillation camera, featuring seven PMTs arranged in a radial pattern coupled to a NaI(Tl) scintillation crystal :cite:p:`Anger1958`. He has also shown that the center of gravity (sometimes still referred to as the Anger method) can be used to reconstruct the positions of gamma interactions in it. The alternative maximum likelihood reconstruction method for scintillation cameras was proposed by Grey and Makovsky in the mid-1970s :cite:p:`gray`. This technique relied on the availability of the light response model for the detector in question. In a few practical implementations that followed, the LRFs were typically obtained by scanning the detector with a well-collimated gamma-ray source.
Implementing such scanning in a noble liquid TPC proved to be an extremely challenging task, so an iterative technique for reconstructing LRFs from calibration data obtained with a non-collimated source was developed by the ZEPLIN III collaboration :cite:p:`solovov_posrec`. This technique, as well as position reconstruction based on iteratively reconstructed LRFs, received the internal name "Mercury" that was later picked up by the LUX :cite:p:`posres_lux_2018` and the LZ collaborations. The current Mercury code is based in part on the position reconstruction module of ANTS2 software package :cite:p:`ANTS2`. Many features implemented in the code were inspired by the work on auto-calibrating gamma cameras :cite:p:`Morozov_2015` :cite:p:`marcos2020self` carried out at LIP-Coimbra between 2012 and 2020.



A little theory
----------
Mercury is a software package that performs reconstruction (i.e. finding the best estimate) of the position of a scintillation vertex from the distribution of the detected scintillation photons across the photo-sensor array(s) in a scintillation detector. It finds the coordinates :math:`(x, y, z)` and the scintillation flash intensity (N) that provide the closest match (similarity) between the vector of measured photo sensor signals :math:`{A_i}` (hit pattern) and the vector of photo sensor responses :math:`{a_i}` calculated for :math:`(x, y, z, N)` according to some model.  Consequently, the following three elements are needed for successful reconstruction:

* A model of the detector response to scintillation events.

* A method for estimating the similarity between the two vectors :math:`{A_i}` and :math:`{a_i}`

* A method for searching the parameter space :math:`(x, y, z, N)` for the optimal similarity (best match)


The light response model
^^^^^^^^^^
A light response function (LRF) defines the response of a photosensor to a scintillation flash of fixed intensity as a function of the position of this flash in the detector. The set of LRFs corresponding to all the active sensors of the detector compose the light response model (LRM). From a practical point of view, an LRM takes :math:`(x, y, z)` as the input and outputs a vector :math:`{a_i}`, calculated for some reference value of the scintillation flash intensity :math:`N_{ref}`. In Mercury, LRFs are implemented as cubic splines in one, two or three dimensions. An important special case is the axially-symmetric LRF, which only depends on the distance from the axis of the sensor in question and can be represented by a 1D spline. The LRFs currently used for S2 position reconstruction in LZ are axially-symmetric. The spline shape is defined by a set of coefficients, which are calculated through a linear fit of the experimental (or simulated) data.

Similarity function
^^^^^^^^^^
This is a function of two vectors, :math:`{A_i}` and :math:`{a_i}`, that returns a single value characterizing the proximity of the two input vectors. This function should be smooth for it to be compatible with gradient-based, iterative optimization algorithms. Mercury currently provides two options for such functions, called (somewhat imprecisely, for historical reasons) maximum likelihood (ML) and least squares (LS). 

* The maximum likelihood method assumes a Poisson distribution for the detected signal and generally performs better for low-intensity events.
* The least squares method assumes a normal distribution for the detected signal. It is equivalent to the ML method for high-intensity events if the uncertainties of the measured signal are dominated by photoelectron statistics. In case the uncertainties have other significant components, the result can be improved by supplying them externally.

Optimization method
^^^^^^^^^^
Mercury uses the `CERN Minuit2 <https://root.cern.ch/root/htmldoc/guides/minuit2/Minuit2.html>`_ package to search for the optimum of the similarity function. The default method is a gradient search (Migrad), while Simplex can be used as an alternative (these two are the ones proven to work best, but there are others). Because the similarity function can have multiple local minima, it is important to initialize the optimization algorithm with a good starting point (initial guess) that is reasonably close to the actual event position. The initial guess can be calculated internally as a center of gravity (CoG) of the signal distribution across the sensor array or supplied externally.

Contents
--------

.. toctree::
   index
   usage
   lrm
   splines
   lrm_advanced
   jsonstructure
   api


Bibliography
--------
.. bibliography::