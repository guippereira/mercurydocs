JSON representation
==================
General structure
----------
Two numbers, representing the total sensor (n_sensors) and group (n_groups) counts plus the arrays that store sensor and group definitions.

.. code-block:: javascript

    {
        "n_sensors": 241,
        "n_groups": 26,
        "sensors": [ ... ],
        "groups": [ ... ]
    }

banana

.. code-block:: javascript
    "n_sensors": 241,
    "n_groups": 26,
    "sensors": [ ... ],
    "groups": [ ... ]
    
Sensor definitions
----------

All sensor types include the following elements: sensor position in the global detector coordinates "x" and "y" (both floats), sensor "id" (non negative integer with zero-based indexing) that is used when addressing the sensor by LRModel methods, "disabled"(boolean) flag that can be used to exclude the sensor from reconstruction, and "gain" (float), that is used to equalize response of the sensors within a group. Sensors that belong to a group have a  non-negative zero-based  integer "group_id" and do not have an LRF defined, as they use the LRF from the respective group. There are three types of sensors with respect to groups:
* A single sensor that is indicated by "group_id": -1, and does not belong to any group. It has its own LRF defined. It does not need a coordinate transform to be defined, as its local coordinate system coincides with the global one (i.e. that of the detector). This particular sensor is placed at the center of the array, since x=y=0.    

    .. code-block:: javascript
        {
            "LRF": { ... },
            "disabled": false,
            "gain": 1,
            "group_id": -1,
            "id": 0,
            "x": 0,
            "y": 0
        } 

* The "root" sensor of a group (namely, group 0 in the example below), whose position and orientation serve as a reference to other sensors in the group. Like for a single sensor, its local coordinates are equal to global ones, so it does not need to define a transform

    .. code-block:: javascript  
        {
            "disabled": false,
            "gain": 1,
            "group_id": 0,
            "id": 1,
            "x": 71.48,
            "y": 41.27
        }

* An ordinary grouped sensor for which a transform needs to be defined to convert global position into local. The transform shown here is a reflection through a line passing through the origin at 60 degrees to the X axis.

.. code-block:: javascript    
    {
        "disabled": false,
        "gain": 1.055,
        "group_id": 0,
        "id": 2,
        "transform": {
            "method": "reflect",
            "phi": 1.0471975511965976
        },
        "x": 0,
        "y": 82.54
    }

Groups definition
----------

Groups are identified by a non-negative integer "id". "x0" and "y0" (both floats) define "root" sensor position. Array "members" contains ids of the sensors that constitute the group. The LRF defined in the group is common to all these sensors.

.. code-block:: javascript
    "groups": [
        {
            "LRF": { ... },
            "id": 0,
            "members": [1, 2, 3, 4, 5, 6],
            "x0": 71.48,
            "y0": 41.27
        },
    ...
    ]
