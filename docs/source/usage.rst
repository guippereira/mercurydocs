From theory to practice
=====

Reconstruction errors
----------
The position reconstruction result makes sense only when it is accompanied by an associated uncertainty (error). This error has two components: systematic and statistical (random). Imagine that we had a sample of events with very high photoelectron statistics (i.e. very small relative statistical fluctuations) and no measurement errors. Even in this case the reconstruction would not be perfect due to the imprecise reproduction of the true light response of the detector by the light response model used in the reconstruction. Systematic error is the deviation of the reconstructed position from the true one in this ideal case. It does not depend on the intensity of the scintillation signal and, consequently, the energy. It can be also called “reconstruction bias” or, especially in nuclear imaging applications, “image nonlinearity” as it leads to distortion of the lines in the image of a bar phantom. 
Now, in a real detector, the measured photo sensor signal is subject to random fluctuations due to various effects, both fundamental like photoelectron statistics and technical like pulse parameterizer uncertainty. Statistical error ― also called the spatial resolution ― characterizes the random deviation, due to these fluctuations, of the reconstructed position from the position that would be reconstructed in the ideal "no fluctuations" case. This deviation becomes smaller with the increase in the intensity of the scintillation signal, approximately following an inverse square root law. To complicate things further, even for signals of fixed intensity, the resolution varies as a function of position. It is also anisotropic, that is, each point in the xy plane is characterized by three parameters (:math:`\sigma_{xx}`, :math:`\sigma_{yy}`, :math:`\sigma_{xy}`).


Disabled sensors
----------
In a real detector, a sensor can malfunction or be switched off for various reasons. Fortunately, this is not a problem for statistical reconstruction since the exclusion of a sensor from the calculation of the similarity function does not have effect on the systematic error. The only impact is a somewhat degraded resolution in the vicinity of the excluded sensor. This degradation would pose a problem only when a cluster of nearby sensors is switched off. In Mercury, the LRM definition stores a “disabled” flag for each sensor, which can be either imported with the rest of the LRM parameters from a JSON file or changed on-the-fly by passing to the appropriate methods a list of sensors to disable or enable.


Input vectors
----------
When Mercury reconstruction has been properly initialized (i.e. the LRM, the similarity function and the optimization method are all correctly defined) the minimal required input for the reconstruction routine is an array with the signals :math:`{A_i}` of all enabled sensors, as long as these sensors stay in linear mode. However, a sensor response can become non-linear (saturate) above a certain threshold. As the reconstruction assumes a linear response for all enabled sensors, saturation leads to a systematic error in the reconstructed values. To tackle this problem, a second input must be supplied to the reconstruction routine along with the array of responses: an array of boolean flags marking the saturated sensors. The sensors marked as saturated are simply excluded from the calculation of the similarity function for the current event. In other words, this boolean array allows to dynamically disable sensors on an event-by-event basis.  


Parameters controlling reconstruction
----------
A good initial guess is essential for a good performance of the reconstruction algorithm. When reconstructing the S2, Mercury takes the initial guess as a center of gravity (CoG), i.e. a sum of the sensor coordinates weighted by their respective signals. A disadvantage of a pure CoG is that it introduces a bias towards the center of the sensor array, which can be especially severe for those events that are far from the center. To partially overcome this problem, Mercury implements a "truncated" CoG, where the weighted sum is taken only over those sensors presenting signals above a certain cutoff threshold. This cutoff can be absolute (:code:`CogAbsCutoff`) or relative to the maximum sensor signal in a given event (:code:`CogRelCutoff`). The latter option tends to have better performance, especially for the datasets with high dynamic range. 
Cutoffs can be applied at the reconstruction stage as well, so that the similarity function is calculated only for the sensors passing the cutoffs. This is done mainly for two reasons: 
* to improve the signal-to-noise ratio and make the reconstruction less sensitive to spurious signals like afterpulsing or random single electrons.
* to make the reconstruction less CPU-intensive, as most of processor cycles are spent calculating the similarity function. 
In the case of S2 reconstruction, the best way to do this is to set the cutoff radius (:code:`RecCutoffRadius`) so that only the sensors within this radius from the initial guess position are used to calculate the similarity function. 


Parameters controlling MINUIT
----------
Minuit has several parameters that control the stopping conditions for the iterations. It assumes that the objective is reached when the similarity function is within a given tolerance value (:code:`RMtolerance`) from the estimated maximum or minimum. It also limits the number of iterations (:code:`RMmaxIterations`) and the objective function calls (:code:`RMmaxFuncCalls`). By default these parameters are set to the values that seem to work reasonably well in general, but one may want to tune them to get better performance for a specific detector or in a specific energy range (possibly, at the expense of a longer running time).


Output values
----------
Always remember that Mercury is not 100% bullet-proof and reconstruction may fail for various reasons, for example, when trying to reconstruct a multiple scatter identified as a single scatter. So the first thing to verify for a newly processed event is its reconstruction status. Zero indicates a success, while any other value indicates that something went wrong with the optimization process and the remaining output values are not valid. The next output value to check is the reduced :math:`\chi^2`, which characterizes how well the observed hit pattern, :math:`{A_i}`, conforms to the single scatter hypothesis. If the reduced :math:`\chi^2` is below the limit (which is event-type specific so I won't put any number here), it's a good indication that the event is in fact a single scatter and the reconstructed variables are valid and can be used in the analysis. These variables in the case of S2 reconstruction are: the position :math:`(x, y)`, the scintillation flash intensity relative to :math:`N_{ref}`, and the errors for the reconstructed position :math:`\sigma_{xx}`, :math:`\sigma_{yy}`, :math:`\sigma_{xy}`.
