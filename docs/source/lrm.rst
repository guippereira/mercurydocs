Light response model
=====

The light response model contains all the information about the detector required by Mercury to perform position reconstruction. This includes the number of photo sensors and then, for each sensor:

* position of the center
* status (enabled/disabled)
* gain
* spatial response in the form of an LRF

Mercury provides helper functions for saving an LRM to (and reading it from) a JSON file. This makes maintenance, exchange and distribution of detector models pretty straightforward.

Light response functions
----------

Depending on the symmetries that characterize the spatial dependence of a sensor response, different types of LRFs can be used. Currently, the following types are supported:

* **R** or "Axial": Axially symmetric LRF with no z dependence. Provides a very good approximation for the S2 response of PMTs in the top array of LZ. Represented internally by a 1D spline.
* **XY**: Smooth LRF with no z dependence. Approximates well the S2 response of the PMTs in the bottom array. Represented internally by a 2D spline.
* **RZ** or **Axial3D**: Axially symmetric LRF with z dependence. Intended for fitting S1 response close to the PMT arrays. Represented internally by a 2D spline.
* **XYZ**: Smooth LRF of all three coordinates. Intended for fitting S1 response.  Represented internally by a 3D spline.

In all cases, the degree of smoothness is controlled by the number of spline intervals in each direction. Additionally, for the axially symmetric variants an optional non-linear transform of the radial coordinate allows for a better fit of the areas where the LRF changes most rapidly (typically, close to the PMT axis) while avoiding overfitting in the smoother regions. The LRFs of different types are completely interchangeable from the point of view of the reconstruction, i.e. an LRM can combine sensors with different types of LRFs, which can be replaced on-the-fly if needed.
An LRF is essentially a wrapper around a spline object. It controls the mapping of the coordinates between the LRF space and the spline space and provides the methods for fitting a sensor response with a spline. It is defined by its type, the minimum limit, the maximum limit and the number of spline intervals along each axis (or radius in case of axial variants), which are directly passed to the underlying spline. When creating an LRM object from a pre-made JSON file, the LRFs come already initialized with the spline coefficients and the LRM can be used right away for position reconstruction. 

LRF fitting
----------

When creating an LRM from scratch, we need to fill the spline coefficients ourselves, which is normally done by fitting the sensor response data. These data are supplied to in the form of a list of 4-vectors :math:`(x, y, z, A)`, each representing a single scatter with coordinates x, y, z and the response recorded for the sensor in question A. For a simple non-binned fit the LRM class provides the :code:`FitNotBinnedData()` method. For a binned fit, first add the data with :code:`AddFitData()` or :code:`AddFitRawData()` and then perform the fit with the :code:`FitSensor()` method. Use :code:`ClearAllFitData()` to discard previously added data for all the sensors in one go. After fitting all the sensors, you can export the LRM as a JSON string and save it to preserve and share the results of your work. 
